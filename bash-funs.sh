#!/bin/bash


# online() ==  0 implies system is online
#          ==  1 implies system is not online
online() {
	nc -z 8.8.8.8 53  >/dev/null 2>&1; echo $?
}


# git-install
# -----------
# Usage:
# git-install <dir> <repo> <branch> <clone_flags> <pull_flags>

# if <dir> exists and online => git -C $dir $pull_flags pull $repo $branch
# if <dir> exists and offline => nothing
# if <dir> doesn't exit and online => git clone -b $branch $clone_flags $repo $dir
# if <dir> doesn't exit and offline => exit 1
git-install() {
	dir=$1
	repo=$2
	branch=$3
	clone_flags=$4
	pull_flags=$5
	
	if [ -d $dir ]; then
		echo "$dir already installed"
		if [ online ]; then
			echo "online"
			echo "pulling ..."
			cmd="git -C $dir $pull_flags pull $repo $branch"
			echo "$cmd"
			$cmd
		else
			echo "offline"
		fi
	elif [ online ]; then
		echo "cloning ..."
		cmd="git clone -b $branch $clone_flags $repo $dir"
		echo "$cmd"
		$cmd
	else
		echo "You need to be online to clone the repository." 2>&1
		exit 1
	fi
}

# targz-install
# -----------
# Usage:
# targz-install <dir> <name> <repo>

# <dir>  = /tmp
# <name> = foo
# <repo> = http://a.com/foo.tar.gz

# if <dir>/<name> exists => already installed
# if <dir> doesn't exit and online => curls, extracts and removes tar file. 
# if <dir> doesn't exit and offline => exit 1

targz-install() {
	dir=$1
	name=$2
	repo=$3
	if [ -d $dir/$name ]; then
		echo "$name already installed"
	elif [ online ]; then
		cmd="cd $dir; wget $repo; tar -zxvf $name.tar.gz"
		echo "cmd=$cmd"
		cd $dir; curl $repo -o $name; tar -zxvf $name.tar.gz; /bin/rm $name.tar.gz
	else
		echo "You need to be online to install the tar-file." 2>&1
		exit 1
	fi
}
